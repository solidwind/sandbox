function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.


% 1. h?(x) = ?0 + ?1 * x1
hx = X * theta;

% 2. hx - y
hx_y = hx - y;

%3. J is 1/2m * (sum of all hx_y^2)
J = sum(hx_y.^2)/2/m;

% =========================================================================

end
