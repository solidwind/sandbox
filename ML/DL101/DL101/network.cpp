#include "network.h"

#include <algorithm>
#include <numeric>
#include <random>
#include <cmath>
#include <ctime>

neuron::ActFuncType neuron::ActFunc = neuron::Tanh;
float neuron::momentum = 0.5;
bool dumpOUt = true;

neuron::neuron(uint32_t numOutputs, uint32_t idxT): idx(idxT){
	weights = vector<float>(numOutputs, 0.);
	deltaWeights = vector<float>(numOutputs, 0.);
	for (size_t i = 0; i < weights.size(); i++) {
		weights[i] = rand() / float(RAND_MAX);
	}
}

void neuron::feedForward(const layer &prevLayer) {
	float z = 0.0;
	for (const auto& prevNeuron : prevLayer){
		z += prevNeuron.getVal() * prevNeuron.getWeights()[idx];
	}

	v = activationFunc(z);
}

float neuron::activationFunc(float z) {
	float v = 0.0;
	switch (ActFunc)
	{
	case neuron::Sigmoid:
		v = 1.0 / (1.0 + exp(-z));
		break;
	case neuron::Tanh:
		v = tanh(z);
		break;
	default:
		break;
	}

	return v;
}

float neuron::derivativeFunc(float x) {
	float v = 0.0;
	switch (ActFunc)
	{
	case neuron::Sigmoid:
		v = x*(1 - x); //g(z)(1-g(z))
		break;
	case neuron::Tanh:
		v = (1.0 - x*x); //1-tanh(z)^2
		break;
	default:
		break;
	}

	return v;
}

void neuron::gradientFunc(float targetV) {
	gradient = (targetV - v) * derivativeFunc(v);
}

void neuron::hiddenGradientFunc(const layer &nextLayer)
{
	double sum = 0.0;
	for (uint32_t i = 0; i < nextLayer.size() - 1; ++i) {
		sum += weights[i] * nextLayer[i].gradient;
	}
	gradient = sum * derivativeFunc(v);
}

void neuron::updateWeights(layer &prevLayer, float eta)
{
	for (auto& neuron : prevLayer){
		double deltaWeight = neuron.deltaWeights[idx];
		deltaWeight = eta * neuron.getVal() * gradient + momentum * deltaWeight;
		neuron.deltaWeights[idx] = deltaWeight;
		neuron.weights[idx] += deltaWeight;
	}
}

network::network(const vector<uint32_t>& layerSizesT,
	const vector<pair<vector<uint32_t>, vector<uint32_t>>>& training_data,
	uint32_t epochsT, uint32_t mini_batch_sizeT, float etaT) 
	: data(training_data), epochs(epochsT), mini_batch_size(mini_batch_sizeT), eta(etaT) {

	srand((unsigned int)time(NULL));
	//setup network
	for (uint32_t i = 0; i < layerSizesT.size(); i++) {
		unsigned numOutputs = (i == layerSizesT.size()-1) ? 0 : layerSizesT[i+1];				
		layer l;
		for (uint32_t j = 0; j < layerSizesT[i] + 1; j++) {
			l.push_back(neuron(numOutputs, j));
		}
		//let's use 1.0 as bias neuron's value
		l.back().setVal(1.0);

		layers.push_back(l);
	}

	if(dumpOUt)
		ofs.open("log.txt", ofstream::trunc | ofstream::out);
}

network::~network(){
	if (dumpOUt)
		ofs.close();
}

void network::train() {
	vector<uint32_t> indices(data.size());
	iota(indices.begin(), indices.end(), 0);

	for(uint32_t i=0; i<epochs; i++){
		//let's not shuffle data, that's too expensive
		//instead, let's shuffle index
		std::shuffle(indices.begin(), indices.end(), std::mt19937{ std::random_device{}() });

		for (uint32_t j = 0; j < data.size(); j++) {
			uint32_t k = j % mini_batch_size;
			if (k == 0) {
				//if last element of current mini_batch, start to train this mini_batch
				train_mini_batch(j);
			}
		}
	}
}

void network::train_mini_batch(uint32_t iStart) {
	for (uint32_t i = iStart; i < mini_batch_size; i++)	{
		feedForward(data[i]);
		backProp(data[i].second);

		if (dumpOUt){
			for (const auto& a : data[i].first)
				ofs << a << " ";
			for (const auto& a : data[i].second)
				ofs << a << " "; //target val
			ofs << layers.back()[0].getVal() << endl; //real value
		}
	}
}

void network::feedForward(const pair<vector<uint32_t>, vector<uint32_t>>& pairData){
	const vector<uint32_t>& ivs = pairData.first;
	const vector<uint32_t>& ovs = pairData.second;
	for (uint32_t i = 0; i < ivs.size(); i++) {
		layers[0][i].setVal(ivs[i]);
	}

	// Forward propagate
	for (unsigned i = 1; i < layers.size(); ++i) {
		layer &prevLayer = layers[i - 1];
		for(uint32_t j=0; j<layers[i].size()-1; j++)
			layers[i][j].feedForward(prevLayer);
	}
}

void network::backProp(const vector<uint32_t>& targetData) {
	totalLoss = 0.0;
	//output layer
	layer& lastL = layers.back();
	uint32_t R = lastL.size(); 
	for (uint32_t i = 0; i < R-1; i++) {
		float e = targetData[i] - lastL[i].getVal();
		totalLoss += e*e;
		lastL[i].gradientFunc(targetData[i]);
	}
	totalLoss = sqrt(totalLoss/(R-1));

	//hidden layers
	for (uint32_t i = layers.size() - 2; i > 0; --i){
		layer &curL = layers[i];
		layer &nextL = layers[i + 1];
		for (uint32_t j = 0; j < curL.size(); ++j) {
			curL[j].hiddenGradientFunc(nextL);
		}
	}

	for (uint32_t i = layers.size() - 1; i > 0; --i){
		layer &curL = layers[i];
		layer &prevL = layers[i - 1];

		for (uint32_t j = 0; j < curL.size() - 1; ++j){
			curL[j].updateWeights(prevL, eta);
		}
	}
}
