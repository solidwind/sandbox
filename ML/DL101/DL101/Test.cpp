// test file

#include "dataPreparator.h"
#include "network.h"

int main() {
	dataPreparator prep(dataPreparator::XOR);
	//prep.generateBLFile();
	const vector<pair<vector<uint32_t>, vector<uint32_t>>>& data = prep.readData();

	vector<uint32_t> layerSizes = { 2, 4, 1 };
	uint32_t epochs = 3;
	uint32_t mini_batch_size = 500;
	float eta = 0.15;
	network nw(layerSizes, data, 3, 500, eta);
	nw.train();

	return 0;
}