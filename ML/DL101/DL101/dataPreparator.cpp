#include "dataPreparator.h"

#include <fstream>
#include <ctime>

dataPreparator::dataPreparator(BLType blOpType):fileName("trainData_"), blType(blOpType){
	string szBLType;
	switch (blType)
	{
	case dataPreparator::AND:
		szBLType = "and";
		break;
	case dataPreparator::OR:
		szBLType = "or";
		break;
	case dataPreparator::NOT:
		szBLType = "not";
		break;
	case dataPreparator::XOR:
		szBLType = "xor";
		break;
	default:
		break;
	}

	fileName = "trainData_";
	fileName = fileName + szBLType + ".txt";
}


dataPreparator::~dataPreparator()
{
}


void dataPreparator::generateBLFile() {
	srand((unsigned int)time(NULL));
	ofstream ofs;
	ofs.open(fileName, ofstream::trunc | ofstream::out);
	for (int i = 2000; i > 0; --i) {
		int i1 = (int)(rand()/double(RAND_MAX)+0.5);
		int i2 = (int)(rand()/double(RAND_MAX)+0.5);
		int o0 = 0;
		switch (blType)
		{
		case dataPreparator::AND:
			o0 = i1 & i2;
			break;
		case dataPreparator::OR:
			o0 = i1 | i2;
			break;
		case dataPreparator::NOT:
			o0 = !i1;
			break;
		case dataPreparator::XOR:
			o0 = i1 ^ i2;
			break;
		default:
			break;
		}
		ofs << i1 << " " << i2 << " " << o0 << endl;
	}
	ofs.close();
}

vector<pair<vector<uint32_t>, vector<uint32_t>>>& dataPreparator::readData() {
	ifstream file;
	file.open(fileName);
	data.clear();

	//hardcode to 2 here temporily
	uint32_t inputSize = 2;
	string line;
	string delim = " ";
	while (getline(file, line)) {
		vector<uint32_t> vI, vO;
		//split line
		auto start = 0U;
		auto end = line.find(delim);
		uint32_t idx = 0;
		while (true){
			uint32_t t = atoi(line.substr(start, end - start).c_str());
			if (idx < inputSize){
				vI.push_back(t);
			}else {
				vO.push_back(t);
			}
			if (end == std::string::npos)break;
			start = end + delim.length();
			end = line.find(delim, start);
			idx++;

		}
		data.push_back(pair<vector<uint32_t>, vector<uint32_t>>(vI, vO));
	}

	file.close();

	return data;
}