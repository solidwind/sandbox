#pragma once

#include <vector>
#include <string>

using namespace std;

class dataPreparator
{
public:
	enum BLType {
		AND,
		OR,
		NOT,
		XOR
	};

	dataPreparator(BLType blType);
	~dataPreparator();

	void generateBLFile();
	vector<pair<vector<uint32_t>, vector<uint32_t>>>& readData();
private:
	string fileName;
	BLType blType;
	//each element is a pair of input values and output vlaues
	vector<pair<vector<uint32_t>, vector<uint32_t>>> data;
};