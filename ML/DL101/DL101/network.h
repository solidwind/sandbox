#pragma once

#include <vector>
#include <fstream>

using namespace std;

class layer;
class neuron {
public:
	enum ActFuncType {
		Sigmoid,
		Tanh
	};

	neuron(uint32_t numOutputs, uint32_t idxT);

	void setVal(float vT) { v = vT; }
	float getVal() const { return v; }
	const vector<float>& getWeights() const { return weights; }

	void feedForward(const layer &prevLayer);
	float activationFunc(float z);
	float derivativeFunc(float x);
	void gradientFunc(float targetV);
	void hiddenGradientFunc(const layer &nextLayer);
	void updateWeights(layer &prevLayer, float eta);

	static ActFuncType ActFunc;
	static float momentum;
private:
	vector<float> weights;
	vector<float> deltaWeights;
	float v;
	uint32_t idx;

	float gradient;
};

class layer : public vector<neuron> {
public:
	layer(uint32_t size, neuron item) :vector(size, item) {}
	layer() {}
};

class network{
public:
	network(const vector<uint32_t>& layerSizes, 
		const vector<pair<vector<uint32_t>, vector<uint32_t>>>& training_data,
		uint32_t epochs, uint32_t mini_batch_size, float eta);
	~network();

	void train();
private:
	void train_mini_batch(uint32_t iStart);
	void feedForward(const pair<vector<uint32_t>, vector<uint32_t>>& pairData);
	void backProp(const vector<uint32_t>& targetData);

	vector<layer> layers;
	const vector<pair<vector<uint32_t>, vector<uint32_t>>>& data;
	uint32_t epochs;
	uint32_t mini_batch_size;
	float eta;

	float totalLoss;
	fstream ofs;
};

